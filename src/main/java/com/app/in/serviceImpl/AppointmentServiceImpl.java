package com.app.in.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.in.dto.AppointmentAddDto;
import com.app.in.dto.IAppointmentDetailDto;
import com.app.in.dto.IAppointmentListDto;
import com.app.in.entities.AppointUser;
import com.app.in.entities.AppointmentEntity;
import com.app.in.entities.UserEntity;
import com.app.in.exceptions.ResourceNotFoundException;
import com.app.in.repositories.AppointUserRepository;
import com.app.in.repositories.AppointmentRepositories;
import com.app.in.service.AppointmentService;
import com.app.in.utils.PaginationUsingFromTo;

@Service
public class AppointmentServiceImpl implements AppointmentService {
	
	private static final Logger LOG = LoggerFactory.getLogger(AppointmentServiceImpl.class);
	
	@Autowired
	private AppointmentRepositories appointmentRepositories;
	
	@Autowired
	private AppointUserRepository appointUserRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public void addAppointment(AppointmentAddDto appointmentBody) {
		UserEntity userEntity = new UserEntity();
		LOG.info("addAppointment(AppointmentAddDto appointmentBody) >> Start");
		AppointmentEntity newAppointment = new AppointmentEntity();
		newAppointment.setName(appointmentBody.getTitle());
		newAppointment.setAppointmentDate(appointmentBody.getAppointmentDate());
		newAppointment.setDescription(appointmentBody.getDescription());
		LOG.info("addAppointment(AppointmentAddDto userBody) >> Done ");
		appointmentRepositories.save(newAppointment);
		LOG.info("addAppointment(AppointmentAddDto userBody) >> Save ");
		return;

	}

	@Override
	public void editAppointment(AppointmentAddDto appointmentBody, Long id) throws ResourceNotFoundException {
		LOG.info("editAppointment(AppointmentAddDto userBody, Long id) >> Start");
		AppointmentEntity newAppointment = appointmentRepositories.findByIdAndIsActiveTrue(id)
				.orElseThrow(() -> new ResourceNotFoundException("Appointment Not Found"));
		newAppointment.setName(appointmentBody.getTitle());
		newAppointment.setAppointmentDate(appointmentBody.getAppointmentDate());
		newAppointment.setDescription(appointmentBody.getDescription());
		LOG.info("editAppointment(AppointmentAddDto userBody, Long id) >> Done");
		appointmentRepositories.save(newAppointment);
		LOG.info("editAppointment(AppointmentAddDto userBody, Long id) >> Save");
		return;


	}

	@Override
	public void deleteAppointment(Long id) throws ResourceNotFoundException {
		LOG.info("deleteAppointment(Long id) >> Start ");
		AppointmentEntity deleteAppointment = appointmentRepositories.findByIdAndIsActiveTrue(id)
				.orElseThrow(() -> new ResourceNotFoundException("Appointment Not Found"));
		deleteAppointment.setIsActive(false);
		LOG.info("deleteAppointment(Long id) >> IsActive = False ");
		LOG.info("deleteAppointment(Long id) >> Delete ");
		appointmentRepositories.save(deleteAppointment);
		LOG.info("deleteAppointment(Long id) >> Save ");
		return;

	}
	
	@Override
	public IAppointmentDetailDto fetchById(Long id) throws ResourceNotFoundException {
		LOG.info("fetchById(Long id) >> Start");
		IAppointmentDetailDto userDetail = appointmentRepositories.findByIdAndIsActiveTrue(id, IAppointmentDetailDto.class)
				.orElseThrow(() -> new ResourceNotFoundException("Appointment Not Found"));
		LOG.info("fetchById(Long id) >> Done");
		return userDetail;
	}

	@Override
	public Page<IAppointmentListDto> fetchAll(String search, String from, String to) {
		LOG.info("fetchAll(String search, String from, String to) >> Start");
		Pageable paging = new PaginationUsingFromTo().getPagination(from, to);
		
		Page<IAppointmentListDto> titleList;
		
		if ((search == "") || (search == null) || (search.length() == 0)) {
			LOG.info("fetchAll(String search, String from, String to) >> If ");
			titleList = appointmentRepositories.findByOrderByIdDesc(paging, IAppointmentListDto.class);
					
			
		} else {
			LOG.info("fetchAll(String search, String from, String to) >> Else");
			titleList = appointmentRepositories.findByNameContainingIgnoreCaseOrderByIdDesc(
					StringUtils.trimLeadingWhitespace(search), paging, IAppointmentListDto.class);
		}
		LOG.info("fetchAll(String search, String from, String to) >> Done ");
		return titleList;

	}

	@Override
	public List<AppointmentAddDto> getAppointUserByAppointment(Long id) throws ResourceNotFoundException {
		// TODO Auto-generated method stub
		AppointUser appointUser = appointUserRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("AppointUser Not Found"));
		List<AppointmentEntity> findByAppointment = appointmentRepositories.findByAppointUser(appointUser);
		
		List<AppointmentAddDto> appointmentDto = findByAppointment.stream().map((appoint) -> this.modelMapper.map(appoint,AppointmentAddDto.class)).collect(Collectors.toList());
		//List<AppointmentAddDto> appointmentDto = findByAppointment.stream().map((appoint) -> this.modelMapper.map(appoint, AppointmentEntity.class)).collect(Collectors.toList());
		return appointmentDto;
	}

}
