package com.app.in.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.in.dto.AppointUserAddDto;
import com.app.in.dto.AppointmentUserDto;
import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.entities.AppointUser;
import com.app.in.entities.AppointmentEntity;
import com.app.in.exceptions.ResourceNotFoundException;
import com.app.in.repositories.AppointUserRepository;
import com.app.in.repositories.AppointmentRepositories;
import com.app.in.service.AppointUserService;
import com.app.in.utils.PaginationUsingFromTo;

@Service
public class AppointUserServiceImpl implements AppointUserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(AppointUserServiceImpl.class);
	
	@Autowired
	private AppointUserRepository appointUserRepository;
	
	@Autowired
	private AppointmentRepositories appointmentRepositories;

//	@Override
//	public void addAppointUser(AppointUserAddDto userBody) {
//		LOG.info("addAppointment(AppointUserAddDto userBody) >> Start");
//		AppointUser newAppointment = new AppointUser();
////		newAppointment.setName(userBody.getName());
//		LOG.info("addAppointment(AppointUserAddDto userBody) >> Done ");
//		appointUserRepository.save(newAppointment);
//		LOG.info("addAppointment(AppointUserAddDto userBody) >> Save ");
//		return;
//	}

	@Override
	public void editAppointUser(AppointUserAddDto userBody, Long id) throws ResourceNotFoundException {
		LOG.info("editAppointment(AppointUserAddDto userBody, Long id) >> Start");
		AppointUser newAppointment = appointUserRepository.findByIdAndIsActiveTrue(id)
				.orElseThrow(() -> new ResourceNotFoundException("Appointment User Not Found"));
		newAppointment.setDeveloperId(userBody.getId());
		LOG.info("editAppointment(AppointUserAddDto userBody, Long id) >> Done");
		appointUserRepository.save(newAppointment);
		LOG.info("editAppointment(AppointUserAddDto userBody, Long id) >> Save");
		return;
	}

	@Override
	public void deleteAppointUser(Long id) throws ResourceNotFoundException {
		LOG.info("deleteAppointUser(Long id) >> Start ");
		AppointUser deleteAppointment = appointUserRepository.findByIdAndIsActiveTrue(id)
				.orElseThrow(() -> new ResourceNotFoundException("Appointment User Not Found"));
		deleteAppointment.setIsActive(false);
		LOG.info("deleteAppointUser(Long id) >> IsActive = False ");
		LOG.info("deleteAppointUser(Long id) >> Delete ");
		appointUserRepository.save(deleteAppointment);
		LOG.info("deleteAppointUser(Long id) >> Save ");
		return;
	}

	@Override
	public IMasterDetailDto fetchById(Long id) throws ResourceNotFoundException {
		LOG.info("fetchById(Long id) >> Start");
		IMasterDetailDto userDetail = appointUserRepository.findByIdAndIsActiveTrue(id, IMasterDetailDto.class)
				.orElseThrow(() -> new ResourceNotFoundException("Appointment User Not Found"));
		LOG.info("fetchById(Long id) >> Done");
		return userDetail;
	}

	@Override
	public Page<IMasterListDto> fetchAll(String search, String from, String to) {
		LOG.info("fetchAll(String search, String from, String to) >> Start");
		Pageable paging = new PaginationUsingFromTo().getPagination(from, to);
		
		Page<IMasterListDto> nameList;
		
		if ((search == "") || (search == null) || (search.length() == 0)) {
			LOG.info("fetchAll(String search, String from, String to) >> If ");
			nameList = appointUserRepository.findByOrderByIdDesc(paging, IMasterListDto.class);
					
			
		} else {
			LOG.info("fetchAll(String search, String from, String to) >> Else");
			nameList = appointUserRepository.findByDeveloperIdContainingIgnoreCaseOrderByIdDesc(
					StringUtils.trimLeadingWhitespace(search), paging, IMasterListDto.class);
		}
		LOG.info("fetchAll(String search, String from, String to) >> Done ");
		return nameList;
	}

	@Override
	public void addAppointUser(AppointmentUserDto userBody, Long id) throws ResourceNotFoundException {
		LOG.info("addAppointment(AppointUserAddDto userBody) >> Start");
		AppointmentEntity appointmentEntity = appointmentRepositories.findByIdAndIsActiveTrue(id)
				.orElseThrow(() -> new ResourceNotFoundException("Appointment Not Found"));
		
		for(Long s : userBody.getId()) {
			AppointUser appointUser= new AppointUser();
//			appointmentEntity.setId(id);
			appointUser.setDeveloperId(s);
//			appointUser.setId(s);
			appointUser.setAppointmentEntity(appointmentEntity);
			appointUserRepository.save(appointUser);
		}		
	}

}
