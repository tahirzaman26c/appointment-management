package com.app.in.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.in.dto.BlockedListDto;
import com.app.in.entities.BlockedList;
import com.app.in.entities.UserEntity;
import com.app.in.repositories.BlockedListRepository;
import com.app.in.service.BlockedListService;
@Service
public class BlockedListServiceImpl implements BlockedListService {
	
	@Autowired
	private BlockedListRepository blockedListRepository;

	@Override
	public void addBlock(BlockedListDto blockList) {
		// TODO Auto-generated method stub
		BlockedList blocks = new BlockedList();
		UserEntity blockUser = new UserEntity();
		blockUser.setId(blockList.getBlocked());//manager id
		blocks.setBlocked(blockUser);//mangaer id
		UserEntity blockByUser = new UserEntity();
		blockByUser.setId(blockList.getBlockedBy());
		blocks.setBlockedBy(blockByUser);
		blockedListRepository.save(blocks);
		return;
	}

}
