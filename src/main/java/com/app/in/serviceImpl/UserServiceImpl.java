package com.app.in.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.dto.UserEntityAddDto;
import com.app.in.entities.UserEntity;
import com.app.in.exceptions.ResourceNotFoundException;
import com.app.in.repositories.UserRepository;
import com.app.in.service.UserService;
import com.app.in.utils.PaginationUsingFromTo;
@Service
public class UserServiceImpl implements UserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public void addUser(UserEntityAddDto userBody) {
		LOG.info("addUser(UserEntityAddDto userBody) >> Start");
		UserEntity newUser = new UserEntity();
		newUser.setName(userBody.getName());
		LOG.info("addUser(UserEntityAddDto userBody) >> Done ");
		userRepository.save(newUser);
		LOG.info("addUser(UserEntityAddDto userBody) >> Save ");
		return;

	}

	@Override
	public void editUser(UserEntityAddDto userBody, Long userId) throws ResourceNotFoundException {
		LOG.info("editUser(UserEntityAddDto userBody, Long userId) >> Start ");
		UserEntity updateUser = userRepository.findByIdAndIsActiveTrue(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User Not Found"));
		updateUser.setName(userBody.getName());
		LOG.info("editUser(UserEntityAddDto userBody, Long userId) >> Done ");
		userRepository.save(updateUser);
		LOG.info("editUser(UserEntityAddDto userBody, Long userId) >> Save ");
		return;

	}

	@Override
	public void deleteUser(Long userId) throws ResourceNotFoundException {
		LOG.info("deleteUser(Long userId) >> Start ");
		UserEntity deleteUser = userRepository.findByIdAndIsActiveTrue(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User Not Found"));
		deleteUser.setIsActive(false);
		LOG.info("deleteUser(Long userId) >> IsActive = False ");
		LOG.info("deleteUser(Long userId) >> Delete ");
		userRepository.save(deleteUser);
		LOG.info("deleteUser(Long userId) >> Save ");
		return;

	}

	@Override
	public IMasterDetailDto fetchById(Long id) throws ResourceNotFoundException {
		LOG.info("fetchById(Long id) >> Start");
		IMasterDetailDto userDetail = userRepository.findByIdAndIsActiveTrue(id, IMasterDetailDto.class)
				.orElseThrow(() -> new ResourceNotFoundException("User Not Found"));
		LOG.info("fetchById(Long id) >> Done");
		return userDetail;
	}

	@Override
	public Page<IMasterListDto> fetchAll(String search, String from, String to) {
		LOG.info("fetchAll(String search, String from, String to) >> Start");
		Pageable paging = new PaginationUsingFromTo().getPagination(from, to);
		
		Page<IMasterListDto> nameList;
		
		if ((search == "") || (search == null) || (search.length() == 0)) {
			LOG.info("fetchAll(String search, String from, String to) >> If ");
			nameList = userRepository.findByOrderByIdDesc(paging, IMasterListDto.class);
					
			
		} else {
			LOG.info("fetchAll(String search, String from, String to) >> Else");
			nameList = userRepository.findByNameContainingIgnoreCaseOrderByIdDesc(
					StringUtils.trimLeadingWhitespace(search), paging, IMasterListDto.class);
		}
		LOG.info("fetchAll(String search, String from, String to) >> Done ");
		return nameList;
	}

}
