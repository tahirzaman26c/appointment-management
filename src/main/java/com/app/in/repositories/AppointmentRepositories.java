package com.app.in.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.in.dto.IAppointmentDetailDto;
import com.app.in.dto.IAppointmentListDto;
import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.entities.AppointUser;
import com.app.in.entities.AppointmentEntity;

@Repository
public interface AppointmentRepositories extends JpaRepository<AppointmentEntity, Long> {
	Optional<AppointmentEntity> findByNameContainingIgnoreCase(String title);

	Optional<AppointmentEntity> findByIdAndIsActiveTrue(Long id);
	
	Optional<IAppointmentDetailDto> findByIdAndIsActiveTrue(Long id,
			Class<IAppointmentDetailDto> IAppointmentDetailDto);
	
	Page<IAppointmentListDto> findByOrderByIdDesc (Pageable page, Class<IAppointmentListDto> IAppointmentListDto);
	
	Page<IAppointmentListDto> findByNameContainingIgnoreCaseOrderByIdDesc(String name, Pageable pageable,
			Class<IAppointmentListDto> IAppointmentListDto);
	
	List<AppointmentEntity> findByAppointUser(AppointUser appointUser);
	
}
