package com.app.in.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.entities.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
	
	Optional<UserEntity> findByNameContainingIgnoreCase(String name);

	Optional<UserEntity> findByIdAndIsActiveTrue(Long userId);
	
	Optional<IMasterDetailDto> findByIdAndIsActiveTrue(Long id,
			Class<IMasterDetailDto> IMasterDetailDto);
	
	Page<IMasterListDto> findByOrderByIdDesc (Pageable page, Class<IMasterListDto> IMasterListDto);
	
	Page<IMasterListDto> findByNameContainingIgnoreCaseOrderByIdDesc(String name, Pageable pageable,
			Class<IMasterListDto> IMasterListDto);

}
