package com.app.in.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.in.entities.BlockedList;

public interface BlockedListRepository extends JpaRepository<BlockedList, Long> {

}
