package com.app.in.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.entities.AppointUser;

@Repository
public interface AppointUserRepository extends JpaRepository<AppointUser, Long> {
//	Optional<AppointUser> findByDeveloperIdContainingIgnoreCase(String title);

	Optional<AppointUser> findByIdAndIsActiveTrue(Long id);
	
	Optional<IMasterDetailDto> findByIdAndIsActiveTrue(Long id,
			Class<IMasterDetailDto> IMasterDetailDto);
	
	Page<IMasterListDto> findByOrderByIdDesc (Pageable page, Class<IMasterListDto> IMasterListDto);
	
	Page<IMasterListDto> findByDeveloperIdContainingIgnoreCaseOrderByIdDesc(String name, Pageable pageable,
			Class<IMasterListDto> IMasterListDto);

}