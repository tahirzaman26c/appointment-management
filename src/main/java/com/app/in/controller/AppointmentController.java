package com.app.in.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.in.dto.AppointmentAddDto;
import com.app.in.dto.ErrorResponseDto;
import com.app.in.dto.IAppointmentDetailDto;
import com.app.in.dto.IAppointmentListDto;
import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.dto.ListResponseDto;
import com.app.in.dto.SuccessResponseDto;
import com.app.in.entities.AppointmentEntity;
import com.app.in.exceptions.ResourceNotFoundException;
import com.app.in.repositories.AppointmentRepositories;
import com.app.in.service.AppointmentService;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {
	
	private static final Logger LOG = LoggerFactory.getLogger(AppointmentController.class);
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private AppointmentRepositories appointmentRepositories;
	
	@PostMapping()
	public ResponseEntity<?> addAppointment(@RequestBody AppointmentAddDto userBody){
		LOG.info("CONTROLLER >> addAppointment() >> userBody ");
		String name = userBody.getTitle();
		
		Optional<AppointmentEntity> dataBaseName = appointmentRepositories.findByNameContainingIgnoreCase(name);
		
		if (dataBaseName.isEmpty()) {
			LOG.info("CONTROLLER >> addAppointment() >> Start >> If ");
			appointmentService.addAppointment(userBody);
			LOG.info("CONTROLLER >> addAppointment() >> Done");
			return new ResponseEntity<>(new SuccessResponseDto("Success", "success", null), HttpStatus.CREATED);
			
		} else {
			LOG.info("CONTROLLER >> addAppointment() >> Exception");
			return new ResponseEntity<>(new ErrorResponseDto("Appointment Already Exist", "appointmentAlreadyExist"),

					HttpStatus.BAD_REQUEST);

		}
		
	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> editApoointment(@Valid @RequestBody AppointmentAddDto userBody,
			@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		LOG.info("CONTROLLER >> appointmentEdits() >> Edits >> userId >> UserBody >> Start");
		try {
			AppointmentEntity userData = appointmentRepositories.findByIdAndIsActiveTrue(id)
					.orElseThrow(() -> new ResourceNotFoundException("first make it the active switch then do the editing"));
			appointmentService.editAppointment(userBody, id);
			LOG.info("CONTROLLER >> appointmentEdits() >> Done");
			return new ResponseEntity<>(new SuccessResponseDto("success", "success",null), HttpStatus.OK);
			
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> appointmentEdits() >> Edits >> Exception");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "Appointment Not Found"),
					HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteAppointment(@PathVariable(value = "id") Long userId)
			throws ResourceNotFoundException {
		LOG.info("CONTROLLER >> deleteAppointment() >> Start");
		try {
			appointmentService.deleteAppointment(userId);
			LOG.info("CONTROLLER >> deleteAppointment() >> Deleted ");
			return new ResponseEntity<>(new SuccessResponseDto("succeess", "success",null), HttpStatus.OK);
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> deleteAppointment() >> Deleted ");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "appointment Not Found"),
					HttpStatus.NOT_FOUND);

		}
	}
	
	
	@GetMapping
	public ResponseEntity<?> getAllAppointment(
			@RequestParam(defaultValue = "") String search, 
			@RequestParam(defaultValue = "1") String pageNo, 
			@RequestParam(defaultValue = "25") String size){
		LOG.info("CONTROLLER >> getAllAppointment() >> Start ");
		
		Page<IAppointmentListDto> users = appointmentService.fetchAll(search, pageNo, size);
		LOG.info("CONTROLLER >> getAllAppointment() >> Done ");
		if (users.getTotalElements() !=0) {
			return new ResponseEntity<>(new SuccessResponseDto("Success", "Success", 
					new ListResponseDto(users.getContent(),users.getTotalElements())), HttpStatus.OK); 
			
		}

		return new ResponseEntity<>(new ErrorResponseDto("Data Not Found", "dataNotFound"), HttpStatus.NOT_FOUND);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getAppointmentById(@PathVariable(value = "id") Long userId) {
		LOG.info("CONTROLLER >> getAppointmentById() >> Start ");
		try {
			IAppointmentDetailDto userDetail = appointmentService.fetchById(userId);
			LOG.info("CONTROLLER >> getAppointmentById() >> Done ");
			return new ResponseEntity<>(new SuccessResponseDto("success", "success", userDetail), HttpStatus.OK);
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> getAppointmentById() >> Exception ");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "appointment Not Found"),
					HttpStatus.NOT_FOUND);
		}
	}
	
	
	@GetMapping("/{id}/get")
	public ResponseEntity<?> getAppointmentUserByAppointment(@PathVariable(value = "id") Long id) throws ResourceNotFoundException{
		
		List<AppointmentAddDto> app = appointmentService.getAppointUserByAppointment(id);
		
		return new ResponseEntity<>(new SuccessResponseDto("Success", "success", app), HttpStatus.OK);
	}

}
