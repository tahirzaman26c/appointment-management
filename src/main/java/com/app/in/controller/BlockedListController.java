package com.app.in.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.in.dto.BlockedListDto;
import com.app.in.dto.SuccessResponseDto;
import com.app.in.entities.BlockedList;
import com.app.in.service.BlockedListService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("/blocked-list")
public class BlockedListController {
	
	@Autowired
	private BlockedListService blockedListService;
	
	@PostMapping
	public ResponseEntity<?> userBlock(@RequestBody BlockedListDto blockedList){
		blockedListService.addBlock(blockedList);
		return new ResponseEntity<>(new SuccessResponseDto("Success", "success", null), HttpStatus.CREATED);
		
	}

}
