package com.app.in.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.in.dto.ErrorResponseDto;
import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.dto.ListResponseDto;
import com.app.in.dto.SuccessResponseDto;
import com.app.in.dto.UserEntityAddDto;
import com.app.in.entities.UserEntity;
import com.app.in.exceptions.ResourceNotFoundException;
import com.app.in.repositories.UserRepository;
import com.app.in.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping()
	public ResponseEntity<?> addUser(@RequestBody UserEntityAddDto userBody){
		LOG.info("CONTROLLER >> addUser() >> userBody ");
		String name = userBody.getName();
		
		Optional<UserEntity> dataBaseName = userRepository.findByNameContainingIgnoreCase(name);
		
		if (dataBaseName.isEmpty()) {
			LOG.info("CONTROLLER >> addUser() >> Start >> If ");
			userService.addUser(userBody);
			LOG.info("CONTROLLER >> addUser() >> Done");
			return new ResponseEntity<>(new SuccessResponseDto("Success", "success", null), HttpStatus.CREATED);
			
		} else {
			LOG.info("CONTROLLER >> addUser() >> Exception");
			return new ResponseEntity<>(new ErrorResponseDto("User Already Exist", "userAlreadyExist"),

					HttpStatus.BAD_REQUEST);

		}
		
	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> editUser(@Valid @RequestBody UserEntityAddDto userBody,
			@PathVariable(value = "id") Long userId) throws ResourceNotFoundException {
		LOG.info("CONTROLLER >> userEdits() >> Edits >> userId >> UserBody >> Start");
		try {
			UserEntity userData = userRepository.findByIdAndIsActiveTrue(userId)
					.orElseThrow(() -> new ResourceNotFoundException("first make it the active switch then do the editing"));
			userService.editUser(userBody, userId);
			LOG.info("CONTROLLER >> userEdits() >> Done");
			return new ResponseEntity<>(new SuccessResponseDto("success", "success",null), HttpStatus.OK);
			
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> userEdits() >> Edits >> Exception");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "User Not Found"),
					HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long userId)
			throws ResourceNotFoundException {
		LOG.info("CONTROLLER >> deleteUser() >> Start");
		try {
			userService.deleteUser(userId);
			LOG.info("CONTROLLER >> deleteUser() >> Deleted ");
			return new ResponseEntity<>(new SuccessResponseDto("succeess", "success",null), HttpStatus.OK);
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> deleteUser() >> Deleted ");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "user Not Found"),
					HttpStatus.NOT_FOUND);

		}
	}
	
	
	@GetMapping
	public ResponseEntity<?> getAllUser(
			@RequestParam(defaultValue = "") String search, 
			@RequestParam(defaultValue = "1") String pageNo, 
			@RequestParam(defaultValue = "25") String size){
		LOG.info("CONTROLLER >> getAllUser() >> Start ");
		
		Page<IMasterListDto> users = userService.fetchAll(search, pageNo, size);
		LOG.info("CONTROLLER >> getAllUser() >> Done ");
		if (users.getTotalElements() !=0) {
			return new ResponseEntity<>(new SuccessResponseDto("Success", "Success", 
					new ListResponseDto(users.getContent(),users.getTotalElements())), HttpStatus.OK); 
			
		}

		return new ResponseEntity<>(new ErrorResponseDto("Data Not Found", "dataNotFound"), HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getUserById(@PathVariable(value = "id") Long userId) {
		LOG.info("CONTROLLER >> getUserById() >> Start ");
		try {
			IMasterDetailDto userDetail = userService.fetchById(userId);
			LOG.info("CONTROLLER >> getUserById() >> Done ");
			return new ResponseEntity<>(new SuccessResponseDto("success", "success", userDetail), HttpStatus.OK);
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> getUserById() >> Exception ");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "user Not Found"),
					HttpStatus.NOT_FOUND);
		}
	}

	

}
