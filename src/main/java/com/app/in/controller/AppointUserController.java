package com.app.in.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.in.dto.AppointUserAddDto;
import com.app.in.dto.AppointmentUserDto;
import com.app.in.dto.ErrorResponseDto;
import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.dto.ListResponseDto;
import com.app.in.dto.SuccessResponseDto;
import com.app.in.entities.AppointUser;
import com.app.in.exceptions.ResourceNotFoundException;
import com.app.in.repositories.AppointUserRepository;
import com.app.in.service.AppointUserService;

@RestController
@RequestMapping("/appointment-user")
public class AppointUserController {
	
	private static final Logger LOG = LoggerFactory.getLogger(AppointUserController.class);
	
	@Autowired
	private AppointUserService appointUserService;
	
	@Autowired
	private AppointUserRepository appointUserRepository;
	
	
//	@PostMapping()
//	public ResponseEntity<?> addAppointUser(@RequestBody AppointUserAddDto userBody){
//		LOG.info("CONTROLLER >> addAppointUser() >> userBody ");
//		String name = userBody.getName();
//		
//		Optional<AppointUser> dataBaseName = appointUserRepository.findByNameContainingIgnoreCase(name);
//		
//		if (dataBaseName.isEmpty()) {
//			LOG.info("CONTROLLER >> addAppointUser() >> Start >> If ");
//			appointUserService.addAppointUser(userBody);
//			LOG.info("CONTROLLER >> addAppointUser() >> Done");
//			return new ResponseEntity<>(new SuccessResponseDto("Success", "success", null), HttpStatus.CREATED);
//			
//		} else {
//			LOG.info("CONTROLLER >> addAppointUser() >> Exception");
//			return new ResponseEntity<>(new ErrorResponseDto("Appointment User Already Exist", "appointmentUserAlreadyExist"),
//
//					HttpStatus.BAD_REQUEST);
//
//		}
//		
//	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> editApoointUser(@Valid @RequestBody AppointUserAddDto userBody,
			@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		LOG.info("CONTROLLER >> appointUserEdits() >> Edits >> userId >> UserBody >> Start");
		try {
			AppointUser userData = appointUserRepository.findByIdAndIsActiveTrue(id)
					.orElseThrow(() -> new ResourceNotFoundException("first make it the active switch then do the editing"));
			appointUserService.editAppointUser(userBody, id);
			LOG.info("CONTROLLER >> appointUserEdits() >> Done");
			return new ResponseEntity<>(new SuccessResponseDto("success", "success",null), HttpStatus.OK);
			
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> appointUserEdits() >> Edits >> Exception");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "Appointment User Not Found"),
					HttpStatus.NOT_FOUND);
		}
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteAppointUser(@PathVariable(value = "id") Long userId)
			throws ResourceNotFoundException {
		LOG.info("CONTROLLER >> deleteAppointUser() >> Start");
		try {
			appointUserService.deleteAppointUser(userId);
			LOG.info("CONTROLLER >> deleteAppointUser() >> Deleted ");
			return new ResponseEntity<>(new SuccessResponseDto("succeess", "success",null), HttpStatus.OK);
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> deleteAppointUser() >> Deleted ");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "appointment User Not Found"),
					HttpStatus.NOT_FOUND);

		}
	}
	
	
	@GetMapping
	public ResponseEntity<?> getAllAppointUser(
			@RequestParam(defaultValue = "") String search, 
			@RequestParam(defaultValue = "1") String pageNo, 
			@RequestParam(defaultValue = "25") String size){
		LOG.info("CONTROLLER >> getAllAppointUser() >> Start ");
		
		Page<IMasterListDto> users = appointUserService.fetchAll(search, pageNo, size);
		LOG.info("CONTROLLER >> getAllAppointUser() >> Done ");
		if (users.getTotalElements() !=0) {
			return new ResponseEntity<>(new SuccessResponseDto("Success", "Success", 
					new ListResponseDto(users.getContent(),users.getTotalElements())), HttpStatus.OK); 
			
		}

		return new ResponseEntity<>(new ErrorResponseDto("Data Not Found", "dataNotFound"), HttpStatus.NOT_FOUND);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getAppointUserById(@PathVariable(value = "id") Long userId) {
		LOG.info("CONTROLLER >> getAppointUserById() >> Start ");
		try {
			IMasterDetailDto userDetail = appointUserService.fetchById(userId);
			LOG.info("CONTROLLER >> getAppointUserById() >> Done ");
			return new ResponseEntity<>(new SuccessResponseDto("success", "success", userDetail), HttpStatus.OK);
		} catch (ResourceNotFoundException e) {
			LOG.info("CONTROLLER >> getAppointUserById() >> Exception ");
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "appointment User Not Found"),
					HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("{id}/create")
	public ResponseEntity<?> addAppointmentToAppointUser(@PathVariable(value = "id") Long id, @Valid @RequestBody AppointmentUserDto userBody)
			throws ResourceNotFoundException {
		try {
			appointUserService.addAppointUser(userBody,id);
			return new ResponseEntity<>(new SuccessResponseDto("succeess", "success",null), HttpStatus.OK);
		} catch (ResourceNotFoundException e) {
			// TODO: handle exception
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "Pincode Not Found"),
					HttpStatus.NOT_FOUND);

		}
		
		
	}
	

}
