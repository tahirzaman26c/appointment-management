package com.app.in.service;

import org.springframework.data.domain.Page;

import com.app.in.dto.AppointUserAddDto;
import com.app.in.dto.AppointmentUserDto;
import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.exceptions.ResourceNotFoundException;

public interface AppointUserService {
	
//	void addAppointUser(AppointUserAddDto userBody);
	
	void editAppointUser(AppointUserAddDto userBody, Long id) throws ResourceNotFoundException;
	
	void deleteAppointUser( Long id) throws ResourceNotFoundException;
	
	IMasterDetailDto fetchById(Long id) throws ResourceNotFoundException;
	
	Page<IMasterListDto> fetchAll(String search, String from, String to);
	
	void addAppointUser(AppointmentUserDto userBody, Long id)throws ResourceNotFoundException;

}
