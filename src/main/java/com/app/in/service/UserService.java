package com.app.in.service;

import org.springframework.data.domain.Page;

import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.dto.UserEntityAddDto;
import com.app.in.exceptions.ResourceNotFoundException;

public interface UserService {
	void addUser(UserEntityAddDto userBody);
	
	void editUser(UserEntityAddDto userBody, Long userId) throws ResourceNotFoundException;
	
	void deleteUser( Long userId) throws ResourceNotFoundException;
	
	IMasterDetailDto fetchById(Long id) throws ResourceNotFoundException;
	
	Page<IMasterListDto> fetchAll(String search, String from, String to);

}
