package com.app.in.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.app.in.dto.AppointmentAddDto;
import com.app.in.dto.IAppointmentDetailDto;
import com.app.in.dto.IAppointmentListDto;
import com.app.in.dto.IMasterDetailDto;
import com.app.in.dto.IMasterListDto;
import com.app.in.exceptions.ResourceNotFoundException;

public interface AppointmentService {
	
	void addAppointment(AppointmentAddDto userBody);
	
	void editAppointment(AppointmentAddDto userBody, Long id) throws ResourceNotFoundException;
	
	void deleteAppointment( Long id) throws ResourceNotFoundException;
	
	IAppointmentDetailDto fetchById(Long id) throws ResourceNotFoundException;
	
	Page<IAppointmentListDto> fetchAll(String search, String from, String to);
	
	List<AppointmentAddDto> getAppointUserByAppointment(Long id) throws ResourceNotFoundException;
	
	

}
