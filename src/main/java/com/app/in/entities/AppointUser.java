package com.app.in.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Appointment_User")
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class AppointUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "Developer_Id")
	private Long developerId;
	
//	@Column(name = "developer_name")
//	private String name;
	
	@Column(name = "is_active")
	private Boolean isActive = true;
	
	@Column(name = "is_Accepted")
	private Boolean isAccepted = false;

	@Column(name = "created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "updated_at")
	@UpdateTimestamp
	private Date updatedAt;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "accepted_date")
	private Date acceptedDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "rejected_date")
	private Date rejectedDate;
	
	@ManyToOne
	@JoinColumn(name = "appointment_id")
	private AppointmentEntity appointmentEntity;

}
