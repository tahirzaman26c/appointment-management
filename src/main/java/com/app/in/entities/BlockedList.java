package com.app.in.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "blocked_list")
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BlockedList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.MERGE)
	private UserEntity blocked;
	
	@OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.MERGE)
	private UserEntity blockedBy;



}
