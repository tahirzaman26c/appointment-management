package com.app.in.dto;

public interface IAppointmentListDto {
	public Long getId();
	public String getName();
	public Boolean getIsActive();

}
