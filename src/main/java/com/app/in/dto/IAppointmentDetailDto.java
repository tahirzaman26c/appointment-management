package com.app.in.dto;

public interface IAppointmentDetailDto {
	public Long getId();
	public String getName();
	public Boolean getIsActive();
	public String getCreatedAt();

}
