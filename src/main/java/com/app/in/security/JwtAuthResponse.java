package com.app.in.security;

import lombok.Data;

@Data
public class JwtAuthResponse {
	
	private String token;

}
